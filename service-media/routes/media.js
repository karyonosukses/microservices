const express = require("express");
const router = express.Router();
const isBase64 = require("is-base64");
const base64Img = require("base64-img");
const fs = require("fs");

// model
const { Media } = require("../models");

router.get("/", async (req, res) => {
  // get database media
  const media = await Media.findAll({
    attributes: ["id", "image"],
  });

  // get filepath
  const mappedMedia = media.map((m) => {
    m.image = `${req.get("host")}/${m.image}`;
    return m;
  });

  // get jika media tidak tersedia di database dan filepath
  if (!mappedMedia.length > 0) {
    return res.status(400).json({
      status: "error",
      message: "Image not found",
    });
  }

  return res.json({
    status: "success",
    data: mappedMedia,
  });
});

router.post("/", (req, res) => {
  const image = req.body.image;

  if (!isBase64(image, { mimeRequired: true })) {
    return res.status(400).json({ status: "error", message: "invalid base64" });
  }

  base64Img.img(image, "./public/images", Date.now(), async (err, filePath) => {
    if (err) {
      return res.status(400).json({ status: "error", message: err.message });
    }

    const filename = filePath.split("\\").pop().split("/").pop();

    const media = await Media.create({ image: `images/${filename}` });

    return res.json({
      status: "success",
      data: {
        id: media.id,
        image: `${req.get("host")}/images/${filename}`,
      },
    });
  });
});

router.delete("/:id", async (req, res) => {
  const id = await req.params.id;
  // get database
  const media = await Media.findByPk(id);

  // get jika media kosong
  if (!media) {
    return res.status(404).json({
      status: "error",
      message: "Media not found",
    });
  }

  // delete link image path
  fs.unlink(`./public/${media.image}`, async (err) => {
    if (err) {
      return res.status(400).json({
        status: "success",
        message: err.message,
      });
    }

    // deleted database file by id
    await media.destroy();

    return res.json({
      status: "success",
      message: "Image Deleted",
    });
  });
});

module.exports = router;
