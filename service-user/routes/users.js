const express = require("express");
var router = express.Router();

const userHendler = require("./hendler/user");

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

router.post("/register", userHendler.register);

module.exports = router;
